# Simple OAuth Enhanced Errors

## Contents of this file

- [Introduction](#introduction)
- [Requirements](#requirements)
- [Installation](#installation)
- [Configuration](#configuration)
- [Maintainers](#maintainers)

## Introduction

The Simple OAuth Enhanced Errors module enhances error messages in responses
from the Simple OAuth module to better indicate the user what went wrong.

This is done by adding a `error` property to the JSON response with a specific
error code.

The following generic error responses are enhanced:

- PasswordGrant when user credentials are incorrect:
  - If the user account has no password set, the error code
    `no_password_set` is added to the response.
  - If the user has been blocked by the `flood` module:
    `user_blocked` is added to the response.
  - If the user's IP address has been blocked by the `flood` module:
    `ip_blocked` is added to the response.

For a full description of the module, visit the [project page](https://www.drupal.org/project/simple_oauth_enhanced_errors).

Use the [Issue queue](https://www.drupal.org/project/issues/simple_oauth_enhanced_errors)
to submit bug reports and feature suggestions, or track changes.

## Requirements

This module requires Drupal 10 and the following modules:

- [Simple OAuth (Version ^6)](https://www.drupal.org/project/simple_oauth)

## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).

### Configuration

The module does not need be configured.

## Maintainers

Current maintainers:

- Christoph Niedermoser ([@nimoatwoodway](https://www.drupal.org/u/nimoatwoodway))
- Christian Foidl ([@chfoidl](https://www.drupal.org/u/chfoidl))

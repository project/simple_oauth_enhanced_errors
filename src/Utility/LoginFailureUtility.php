<?php

namespace Drupal\simple_oauth_enhanced_errors\Utility;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Flood\FloodInterface;
use Drupal\user\UserInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * Utility to determine login failure reason.
 */
class LoginFailureUtility {

  const REASON_IP_BLOCKED = 'ip_blocked';
  const REASON_USER_BLOCKED = 'user_blocked';
  const REASON_NO_PASSWORD_SET = 'no_password_set';

  /**
   * Construct new instance.
   */
  public function __construct(
    protected EntityTypeManagerInterface $entityTypeManager,
    protected ConfigFactoryInterface $configFactory,
    protected FloodInterface $flood,
  ) {}

  /**
   * Try to get reason from request.
   */
  public function determineFromRequest(Request $request): string|null {
    $username = $request->get('username');
    if (!$username) {
      return NULL;
    }

    $user = $this->getUser($username);

    $blockedReason = $this->checkFloodBlock($user, $request);
    if ($blockedReason) {
      return $blockedReason;
    }

    // All generic reasons handled.
    if (!$user) {
      return NULL;
    }

    $reason = $this->checkUserNoPassword($user);
    if ($reason) {
      return $reason;
    }

    return NULL;
  }

  /**
   * Checks if the login failed due to ip or user being blocked.
   */
  protected function checkFloodBlock(UserInterface|null $user, Request $request) {
    $floodConfig = $this->configFactory->get('user.flood');

    // Check if IP is blocked.
    if (!$this->flood->isAllowed('oauth2_password_grant.failed_login_ip', $floodConfig->get('ip_limit'), $floodConfig->get('ip_window'))) {
      return self::REASON_IP_BLOCKED;
    }

    if (!$user) {
      return NULL;
    }

    if ($floodConfig->get('uid_only')) {
      // Register flood events based on the uid only, so they apply for any
      // IP address. This is the most secure option.
      $identifier = $user->id();
    }
    else {
      // The default identifier is a combination of uid and IP address. This
      // is less secure but more resistant to denial-of-service attacks that
      // could lock out all users with public user names.
      $identifier = $user->id() . '-' . $request->getClientIP();
    }

    // Check if user is blocked.
    if (!$this->flood->isAllowed(
      'oauth2_password_grant.failed_login_user',
      $floodConfig->get('user_limit'),
      $floodConfig->get('user_window'),
      $identifier
    )) {
      return self::REASON_USER_BLOCKED;
    }

    return NULL;
  }

  /**
   * Check if user account has not password set.
   */
  protected function checkUserNoPassword(UserInterface $user) {
    if (is_null($user->getPassword())) {
      return self::REASON_NO_PASSWORD_SET;
    }

    return NULL;
  }

  /**
   * Get an active user by username or email.
   *
   * @param string $usernameOrEmail
   *   The username or email address.
   *
   * @return \Drupal\user\UserInterface|null
   *   The user or NULL if not found.
   */
  protected function getUser(string $usernameOrEmail): ?userInterface {
    // If username contains @, search for user by email first.
    if (strpos($usernameOrEmail, '@') !== FALSE) {
      $user = $this->getUserByProperty('mail', $usernameOrEmail);
      if ($user) {
        return $user;
      }
    }

    return $this->getUserByProperty('name', $usernameOrEmail);
  }

  /**
   * Get an active user by property.
   *
   * @param string $property
   *   The property to search for.
   * @param string $value
   *   The value to search for.
   *
   * @return \Drupal\user\UserInterface|null
   *   The user or NULL if not found.
   */
  protected function getUserByProperty(string $property, string $value): ?UserInterface {
    $userSearch = \Drupal::entityTypeManager()
      ->getStorage('user')
      ->loadByProperties([$property => $value, 'status' => 1]);

    if ($user = reset($userSearch)) {
      /** @var \Drupal\user\UserInterface $user */
      return $user;
    }

    return NULL;
  }

}

<?php

declare(strict_types=1);

namespace Drupal\simple_oauth_enhanced_errors\EventSubscriber;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Url;
use Drupal\simple_oauth_enhanced_errors\Utility\LoginFailureUtility;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * This event subscriber listenes on kernel events.
 */
class KernelEventSubscriber implements EventSubscriberInterface {

  /**
   * Construct a new instance.
   */
  public function __construct(
    protected LoginFailureUtility $loginFailureUtility,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      KernelEvents::RESPONSE => 'onKernelResponse',
    ];
  }

  /**
   * Handle kernel response event.
   */
  public function onKernelResponse(ResponseEvent $event): void {
    $tokenPath = Url::fromRoute('oauth2_token.token')->toString();

    $response = $event->getResponse();
    $pathname = $event->getRequest()->getPathInfo();

    // Do not handle non-error responses.
    if ($response->getStatusCode() < 400) {
      return;
    }

    // Handle /oauth/token response.
    if ($pathname === $tokenPath) {
      $this->handleTokenResponse($event);
      return;
    }
  }

  /**
   * Handle token response.
   */
  protected function handleTokenResponse(ResponseEvent $event): void {
    $payload = $this->getResponsePayload($event);

    $error = is_array($payload) ? $payload['error'] : '';
    $message = is_array($payload) ? $payload['message'] : '';

    if ($error === "invalid_grant" && str_contains($message, 'user credentials were incorrect')) {
      $this->handleInvalidCredentials($event);
      return;
    }
  }

  /**
   * Handle a invalid credentials response.
   *
   * This response could have the following reasons:
   * - Username and/or password is incorrect.
   * - User or IP was blocked for logins.
   * - User logs in, but no password is set on account.
   */
  protected function handleInvalidCredentials(ResponseEvent $event): void {
    $request = $event->getRequest();

    $reason = $this->loginFailureUtility->determineFromRequest($request);

    if ($reason) {
      $this->mergeResponsePayload($event, [
        'error_code' => $reason,
      ]);
    }
  }

  /**
   * Extract json payload from response.
   */
  protected function getResponsePayload(ResponseEvent $event): array|null {
    $content = $event->getResponse()->getContent();
    if (!$content) {
      return NULL;
    }

    try {
      return Json::decode($content);
    }
    catch (\Exception) {
      return NULL;
    }
  }

  /**
   * Adds given data to existing json response payload.
   */
  protected function mergeResponsePayload(ResponseEvent $event, array $additionalData) {
    $payload = $this->getResponsePayload($event) ?? [];

    $newPayload = Json::encode(array_merge($payload, $additionalData));

    $event->getResponse()->setContent($newPayload);
  }

}

<?php

namespace Drupal\Tests\simple_oauth_enhanced_errors;

use Drupal\Component\Serialization\Json;
use Drupal\Core\Flood\FloodInterface;
use Drupal\KernelTests\Core\Entity\EntityKernelTestBase;
use Drupal\simple_oauth_enhanced_errors\EventSubscriber\KernelEventSubscriber;
use Drupal\simple_oauth_enhanced_errors\Utility\LoginFailureUtility;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Test login failure errors.
 */
class LoginFailureTest extends EntityKernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'consumers',
    'simple_oauth',
    'serialization',
    'simple_oauth_enhanced_errors',
  ];

  /**
   * The event subscriber.
   */
  protected KernelEventSubscriber $subscriber;

  /**
   * The kernel.
   */
  protected HttpKernelInterface $httpKernel;

  /**
   * Flood service.
   */
  protected FloodInterface $flood;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->installConfig(['user']);

    $this->subscriber = $this->container->get('simple_oauth_enhanced_errors.event_subscriber.kernel');
    $this->httpKernel = $this->container->get('http_kernel');

    $this->flood = $this->container->get('flood');

    $this->flood->clear('oauth2_password_grant.failed_login_ip');
    $this->flood->clear('oauth2_password_grant.failed_login_user');
  }

  /**
   * Test no password.
   */
  public function testNoPassword() {
    $user = $this->drupalCreateUser(
      values: [
        'pass' => NULL,
      ]
    );

    $event = $this->createResponseEvent($user->getAccountName());

    $this->subscriber->onKernelResponse($event);

    $this->assertStringContainsString(LoginFailureUtility::REASON_NO_PASSWORD_SET, $event->getResponse()->getContent());
  }

  /**
   * Test user blocked.
   */
  public function testUserBlocked() {
    $user = $this->drupalCreateUser();
    $event = $this->createResponseEvent($user->getAccountName());

    $this->subscriber->onKernelResponse($event);
    $this->assertStringNotContainsString(LoginFailureUtility::REASON_USER_BLOCKED, $event->getResponse()->getContent());

    $floodConfig = $this->config('user.flood');
    $count = $floodConfig->get('user_limit');

    $identifier = $floodConfig->get('uid_only') ? $user->id() : $user->id() . '-127.0.0.1';

    for ($i = 0; $i < $count; $i++) {
      $this->flood->register('oauth2_password_grant.failed_login_user', 3600, $identifier);
    }

    $this->subscriber->onKernelResponse($event);
    $this->assertStringContainsString(LoginFailureUtility::REASON_USER_BLOCKED, $event->getResponse()->getContent());
  }

  /**
   * Test ip blocked.
   */
  public function testIpBlocked() {
    $user = $this->drupalCreateUser();
    $event = $this->createResponseEvent($user->getAccountName());

    $this->subscriber->onKernelResponse($event);
    $this->assertStringNotContainsString(LoginFailureUtility::REASON_IP_BLOCKED, $event->getResponse()->getContent());

    $floodConfig = $this->config('user.flood');
    $count = $floodConfig->get('ip_limit');

    for ($i = 0; $i < $count; $i++) {
      $this->flood->register('oauth2_password_grant.failed_login_ip');
    }

    $this->subscriber->onKernelResponse($event);
    $this->assertStringContainsString(LoginFailureUtility::REASON_IP_BLOCKED, $event->getResponse()->getContent());
  }

  /**
   * Create response event.
   */
  protected function createResponseEvent(string $username) {
    $request = Request::create(
      uri: '/oauth/token',
      method: 'POST',
      parameters: [
        'username' => $username,
      ]
    );

    $response = new Response(Json::encode([
      'error' => 'invalid_grant',
      'message' => 'The user credentials were incorrect',
    ]), 403);

    return new ResponseEvent($this->httpKernel, $request, 0, $response);
  }

}
